using System;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;


namespace shipshooter
{
	public class Particle
	{
		Vector4 color;
		Vector4 startColor;
		Vector4 endColor;
		TimeSpan endtime = TimeSpan.Zero;
		TimeSpan lifetime;

		public Vector3 position;
		Vector3 velocity;
		protected Vector3 acceleration = new Vector3(1.0f,1.0f,1.0f);


		public bool delete;

		public Vector4 Color
		{
			get
			{ 
				return color;
			}
		}

		public Particle (Vector2 position,Vector2 velocity, Vector4 startColor, Vector4 endColor, TimeSpan lifetime)
		{
			this.position = new Vector3(position,0.0f);
			this.velocity = new Vector3(velocity,0.0f);
			this.startColor = startColor;
			this.endColor = endColor;
			this.lifetime = lifetime;
		}

		public void Update(TimeSpan time,TimeSpan elapsedTime)
		{
			if(endtime == TimeSpan.Zero)
				endtime = time + lifetime;
			if(time > endtime)
				delete = true;
			float percentLife = (float) ((endtime.TotalSeconds - time.TotalSeconds)/lifetime.TotalSeconds);

			color = Vector4.Lerp(endColor,startColor,percentLife);

			velocity += Vector3.Multiply(acceleration,(float) elapsedTime.TotalSeconds);

			position += Vector3.Multiply(velocity,(float) elapsedTime.TotalSeconds);
		}

		public bool Delete
		{
			get {return delete;}
			set {delete = value;}
		}
	}
}

