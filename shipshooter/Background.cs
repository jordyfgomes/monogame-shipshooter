using System;
using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace shipshooter
{
	public class Background
	{

		List<Star> starsList = new List<Star>();
		List<Star> inactiveStarsList = new List<Star>();
		Random r = new Random();
		ContentManager Content;
	
		int windowHEIGHT;

		public Background (ContentManager Content,int windowWIDTH,int windowHEIGHT)
		{
			this.windowHEIGHT = windowHEIGHT;
			this.Content = Content;
			for(int i = 0; i < 220; i++)
			{
				starsList.Add(new Star(Content,r.Next(800),r.Next(600),windowHEIGHT,(float) r.NextDouble()));
			}
		}

		public void Update(GameTime gametime)
		{


			foreach(Star star in starsList)
			{
				star.Update();
				if(!star.Active)
					inactiveStarsList.Add(star);
			}

			foreach(Star star in inactiveStarsList)
				starsList.Remove(star);

			inactiveStarsList = new List<Star>();
			starsList.Add(new Star(Content,r.Next(800),-20,windowHEIGHT,(float) r.NextDouble()));
		
		}

		public void Draw(SpriteBatch batch)
		{

			foreach(Star star in starsList)
			{

				star.Draw(batch);
			}
		}
	}
}

