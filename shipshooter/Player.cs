using System;
using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;


namespace shipshooter
{
	public class Player
	{
		Texture2D shipSprite;
		Vector2 spriteOrigin;
		Rectangle drawRectangle;

		int windowWidth;
		int xVel;
		int lives;

		//Blink var 
		int blinkCounter = 0;
		int timeVar = 0;
		bool blinkAux;

		Explosion explosion;

		bool exploding;
		bool active;
		bool blinking;
		bool canRespawn;

		SoundEffectInstance explosionSoundInstance;

		public Player (Vector2 position, String spriteloc, ContentManager content,int windowWidth,int windowHeight, GraphicsDevice gd, SoundEffectInstance explosionSoundInstance)
		{
			shipSprite =  content.Load<Texture2D>(spriteloc);

			spriteOrigin.X = (float) shipSprite.Width/2.0f;
			spriteOrigin.Y = (float) shipSprite.Height/2.0f;

			this.explosionSoundInstance = explosionSoundInstance;

			//bools for control exploding and collision
			exploding = false;
			active = false;
			blinking = true;
			blinkAux = true;
			canRespawn = true;

			//Blink coun
			//this.windowHeight = windowHeight;
			this.windowWidth = windowWidth;
			explosion = new Explosion(content);
	
			drawRectangle = new Rectangle((int) position.X - shipSprite.Width/2, (int) position.Y - shipSprite.Height/2, shipSprite.Width, shipSprite.Height);

			lives = 3;

		}

		public void Update(GameTime gametime)
		{

		
			drawRectangle.X += xVel; 
			if(drawRectangle.Left < 0)
			{
				drawRectangle.X += 2;
			}
			else if(drawRectangle.Right > windowWidth)
			{
				drawRectangle.X -= 2;
			}
			xVel = 0;
			if(explosion.Playing)
			{
				explosion.Update(gametime);
				if(!explosion.Playing && lives >= 0 && !blinking)
				{
					//active = true;
					blinkCounter = 0;
					exploding = false;
					blinking = true;
				}
			}
			if(blinking)
			{
				timeVar = (++timeVar)%20;
				if(timeVar == 10)
				{
					blinkAux = !blinkAux;
					blinkCounter++;
					if(blinkCounter > 8)
					{
						blinking = false;
						active = true;
						blinkAux = false;
					}
				}
			}
		}

		public void UpdateInput(KeyboardState keyState)
		{
			if(keyState.IsKeyDown(Keys.Left))
			{
				TurnLeft();
			}
			else if(keyState.IsKeyDown(Keys.Right))
			{
				TurnRight();
			}
		}

		private void TurnLeft()
		{
			xVel = -4;
		}

		private void TurnRight()
		{
			xVel = 4;
		}

		public void Draw(SpriteBatch spriteb)
		{
			if(blinkAux)
				spriteb.Draw(shipSprite,drawRectangle,Color.White);
			else
			{
				if(Active)
				{
					spriteb.Draw(shipSprite,drawRectangle,Color.White);
				}
				else
				{
					if(exploding)
					{
						explosion.Play(drawRectangle.Center.X,drawRectangle.Center.Y);
						exploding = false;
					}
					explosion.Draw(spriteb);

				}

			}
	
		}

		public bool Exploding
		{
			get {return exploding;}
			set {exploding = value;}
		}

		public Rectangle getDrawRectangle()
		{
			return drawRectangle;
		}

		public bool Active
        {
            get { return active; }
            set { active = value; }
        }

		public int takeLife()
		{
			explosionSoundInstance.Play();
			return --lives;
		}

		public int Lives
		{
			get {return lives;}
		}

		public bool Bliking
		{
			get {return blinking;}
		}

		public bool CanRespawn
		{
			get {return canRespawn;}
		}

	}
}

