using System;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace shipshooter
{
	public class Star
	{
		Texture2D texture;
		Rectangle drawRectangle;
		bool active;
		int windowHeight;
		static int yVel = 2;
		Color color;
		float scale;
		Vector2 position,origin;

		public Star (ContentManager Content, int X, int Y,int windowHeight, float scale)
		{
			texture = Content.Load<Texture2D>("star");
			drawRectangle = texture.Bounds;
			active = true;
			this.windowHeight = windowHeight;
			drawRectangle.X = 0;
			drawRectangle.Y = 0;

			this.scale = scale;

			position = new Vector2(X,Y);
			origin = new Vector2(drawRectangle.Center.X,drawRectangle.Center.Y);

			if(X % 7 == 0)
			{
				switch(X % 5)
				{
				case 1:
					color = Color.Red;
					break;
				case 2:
					color = Color.BlueViolet;
					break;
				case 3:
					color = Color.DarkSeaGreen;
					break;
				case 4:
					color = Color.Yellow;
					break;
				case 5:
					color = Color.Gold;
					break;
				}
			}
			else
			{
				this.scale = scale/2;
				color = Color.White;

			}				
		}

		public void Update()
		{
			position.Y += yVel;
			if(position.Y > windowHeight)
			{
				active = false;
			}
		}

		public void Draw(SpriteBatch spriteb)
		{

			spriteb.Draw(texture,position,drawRectangle,color,0f,origin,scale,SpriteEffects.None,0);
		}

		public bool Active
		{
			get {return active;}
			set {active = value;}
		}
	}
}

