using System;
using System.Security.Cryptography;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace shipshooter
{
	public class Enemy
	{		
		Texture2D enemySprite;
		Vector2 spriteOrigin;
		Rectangle drawRectangle;
		Rectangle playerRec;
		Random r = new Random();

		Color color;

		int windowHeight;
		int yVel;

		RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
		bool active;

		byte[] vb = new byte[1];

		private int devolveAlertorio()
		{
			rng.GetBytes(vb);

			int c = (int) (vb[0] + (r.NextDouble()*485));

			rng.Dispose();

			if(c < 64)
				c += 64;
			else if(c > windowHeight - 64)
				c -= 64;
			return c;
		} 

		public Enemy (String spriteloc, ContentManager content,int windowWidth,int windowHeight)
		{
			enemySprite =  content.Load<Texture2D>(spriteloc);

			spriteOrigin.X = (float) enemySprite.Width/2.0f;
			spriteOrigin.Y = (float) enemySprite.Height/2.0f;

			Active = true;

			color = pickcolor();

			this.windowHeight = windowHeight;

			yVel = 3;

			drawRectangle = new Rectangle(devolveAlertorio() - enemySprite.Width/2,-40 - enemySprite.Height/2, enemySprite.Width, enemySprite.Height);

		}

		public Color pickcolor()
		{
			switch(r.Next(10))
			{
			case 0:
				return Color.White;
			case 1:
				return Color.Red;
			case 2:
				return Color.Silver;
			case 3:
				return Color.Violet;
			case 4:
				return Color.Yellow;
			case 5:
				return Color.Green;
			case 6:
				return Color.Gold;
			case 7:
				return Color.Orange;
			case 8:
				return Color.Chocolate;
			case 9:
				return Color.Cyan;
			default: return Color.GhostWhite;
			}
		}

		public void Update (GameTime gametime,Player ship)
		{
			playerRec = ship.getDrawRectangle();
			drawRectangle.Y += yVel;
			if (drawRectangle.Top > windowHeight) 
			{
				Active = false;
			}
			else if(drawRectangle.Intersects(playerRec) && ship.Active)
			{
				active = false;
				ship.Active = false;
				ship.takeLife();
				ship.Exploding = true;
			}
		}

		public void Draw(SpriteBatch spriteb)
		{
			spriteb.Draw(enemySprite,drawRectangle,color);
		}

		public Rectangle getDrawRetangle()
		{
			return drawRectangle;
		}

		public bool Active
        {
            get { return active; }
            set { active = value; }
        }

		public Vector2 getPosition()
		{
			Vector2 position = new Vector2(drawRectangle.Center.X,drawRectangle.Center.Y);
			return position;

		}
	}
}

