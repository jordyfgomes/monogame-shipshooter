#region Using Statements
using System;
using System.Threading;
using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

#endregion

namespace shipshooter
{

	/// <summary>
	/// This is the main type for your game
	/// </summary>
	public class Game1 : Game
	{
		GraphicsDeviceManager graphics;
		SpriteBatch spriteBatch;

		int window_WIDTH = 800;
	 	int window_HEIGHT = 600;


		//sounds
		SoundEffect explosion_sound;
		SoundEffectInstance explosionSoundInstance;

		//Music
		SoundEffect menu_music;
		SoundEffectInstance menuMusicInstance;
		//Music
		SoundEffect main_music;
		SoundEffectInstance mainMusicInstance;

		//Objetos gráficos
		Player ship;
		Background map;

		//bool para controlar os spawns
		bool spawnTime;
		//bool para controlar os tiros
		bool canFire;
		double timer;

		//Fontype for game
		SpriteFont font;

		//Particle system
		basicParticleSystem particleSystem;

		String strg = "Shipshooter, by Jordy";


		//Frame Rate Counter
		FrameRateCounter fpscounter;


		//Gamestate
		public enum GameState
		{
			StartScreen,
			Running,
			EndScreen
		}

		GameState gamestate = GameState.StartScreen;

		//Audio variables
		SoundEffect snd_shoot;
		SoundEffectInstance snd_shootInstance;

		//Lista de objetos usados no jogo
		List<Enemy> enemies = new List<Enemy>();
		List<Enemy> toRemove = new List<Enemy>();
		List<Shoot> shootsFired = new List<Shoot>();
		List<Shoot> shootsToRemove = new List<Shoot>();

		KeyboardState keyState;

		public Game1 ()
		{

			graphics = new GraphicsDeviceManager (this);
			Content.RootDirectory = "Content";
			graphics.PreferredBackBufferWidth = window_WIDTH;
			graphics.IsFullScreen = false;
			graphics.PreferredBackBufferHeight = window_HEIGHT;
			graphics.ApplyChanges();
		}

		/// <summary>
		/// Allows the game to perform any initialization it needs to before starting to run.
		/// This is where it can query for any required services and load any non-graphic
		/// related content.  Calling base.Initialize will enumerate through any components
		/// and initialize them as well.
		/// </summary>
		protected override void Initialize ()
		{

			spawnTime = true;
			canFire = true;
			// TODO: Add your initialization logic here
			base.Initialize();
				
		}

		/// <summary>
		/// LoadContent will be called once per game and is the place to load
		/// all of your content.
		/// </summary>
		protected override void LoadContent ()
		{
			// Create a new SpriteBatch, which can be used to draw textures.
			spriteBatch = new SpriteBatch (GraphicsDevice);
			snd_shoot = Content.Load<SoundEffect>("sounds/shoot");
			explosion_sound = Content.Load<SoundEffect>("sounds/explosion");
			menu_music = Content.Load<SoundEffect>("sounds/spaceAbyssMenu");
			main_music = Content.Load<SoundEffect>("sounds/space_abyss_full_theme");
			menuMusicInstance = menu_music.CreateInstance();
			explosionSoundInstance = explosion_sound.CreateInstance();
			snd_shootInstance = snd_shoot.CreateInstance();
			mainMusicInstance = main_music.CreateInstance();
			snd_shootInstance.Volume = 0.75f;
			explosionSoundInstance.Volume = 0.75f;
			menuMusicInstance.Volume = 0.75f;
			mainMusicInstance.Volume = 0.75f;

			mainMusicInstance.IsLooped = true;
			menuMusicInstance.IsLooped = true;
			
			//TODO: use this.Content to load your game content here 
			ship = new Player(new Vector2(100,window_HEIGHT - 40),"ship",Content,window_WIDTH,window_HEIGHT,this.GraphicsDevice,explosionSoundInstance);

			map = new Background(Content,window_HEIGHT,window_WIDTH);

			//font
			font = Content.Load<SpriteFont>("arial");
			fpscounter = new FrameRateCounter(font);



			//load circle particle system
			particleSystem = new basicParticleSystem(Content.Load<Texture2D>("circle"));


		}



		/// <summary>
		/// Allows the game to run logic such as updating the world,
		/// checking for collisions, gathering input, and playing audio.
		/// </summary>
		/// <param name="gameTime">Provides a snapshot of timing values.</param>
		protected override void Update (GameTime gameTime)
		{
			fpscounter.Update (gameTime);
			if (menuMusicInstance.State == SoundState.Stopped && gamestate == GameState.StartScreen)
				menuMusicInstance.Play ();
			else if (gamestate == GameState.Running && mainMusicInstance.State == SoundState.Stopped) 
			{
				menuMusicInstance.Stop ();
				mainMusicInstance.Play ();
			}
			// For Mobile devices, this logic will close the Game when the Back button is pressed
//			if (GamePad.GetState (PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
//			{
//				this.Exit ();
//			}
			//Keyboard shit
			keyState = Keyboard.GetState ();
			if (keyState.IsKeyDown(Keys.Escape))
			{
				disposeSounds();
				Exit ();
			}
			else if (keyState.IsKeyDown(Keys.Space))
			{
				switch(gamestate)
				{
					case GameState.Running:
					if(canFire && (ship.Active || ship.Bliking)) 
						{
							shootsFired.Add (new Shoot (new Vector2 (ship.getDrawRectangle ().Center.X, ship.getDrawRectangle ().Center.Y), Content, window_HEIGHT));
							timer = gameTime.TotalGameTime.TotalMilliseconds;
							canFire = false;
							snd_shootInstance.Play();
						} else if (gameTime.TotalGameTime.TotalMilliseconds - timer >= 500f)
							canFire = true;
						break;
					case GameState.StartScreen:
					 	gamestate = GameState.Running;
						break;
				}
					

			}

			//If the game is running
			if(gamestate == GameState.Running)
			{
				//Update objects

				map.Update(gameTime);
				ship.UpdateInput (keyState);
				ship.Update (gameTime);
				particleSystem.Update(gameTime.TotalGameTime, gameTime.ElapsedGameTime);
				//Spawn Enemies
				if (gameTime.TotalGameTime.Seconds % 2 == 0 && spawnTime)
				{
					spawnTime = false;
					enemies.Add (new Enemy ("enemy", Content, window_WIDTH, window_HEIGHT));
				}

				if(gameTime.TotalGameTime.Seconds % 2 != 0)
				{
					spawnTime = true;
				}
				//Shoots and enemies
				foreach(Shoot shoot in shootsFired)
				{
					shoot.Update(gameTime);
					foreach(Enemy enemy in enemies)
						if( shoot.getDrawRetangle().Intersects( enemy.getDrawRetangle() ) )
						{
							shoot.Active = false;
							enemy.Active = false;
							particleSystem.AddExplosion(enemy.getPosition());
							explosionSoundInstance.Play();

						}
					if(!shoot.Active)
						shootsToRemove.Add(shoot);
				}
				foreach (Enemy enemy in enemies) {
					enemy.Update (gameTime,ship);
					if(!enemy.Active)
						toRemove.Add(enemy);
				}
				//remove useless enemy objects
				foreach(Enemy enemy in toRemove)
				{

					enemies.Remove(enemy);
				}
				foreach(Shoot shoot in shootsToRemove)
				{
					shootsFired.Remove(shoot);
				}

				toRemove = new List<Enemy>();
				shootsToRemove = new List<Shoot>();

			}
			// TODO: Add your update logic here			
			base.Update (gameTime);

		
		}

		/// <summary>
		/// This is called when the game should draw itself.
		/// </summary>
		/// <param name="gameTime">Provides a snapshot of timing values.</param>
		protected override void Draw (GameTime gameTime)
		{

		
			graphics.GraphicsDevice.Clear (Color.Black);
			//TODO: Add your drawing code here
			spriteBatch.Begin();
			fpscounter.Draw (gameTime, spriteBatch);
			if (gamestate == GameState.StartScreen)
			{
				spriteBatch.DrawString (font,strg, new Vector2 (100, 100),Color.White,0f,new Vector2(0,0),2.0f,SpriteEffects.None,0.0f);
				spriteBatch.DrawString (font,"Press Space to Continue", new Vector2 (100,window_HEIGHT - window_HEIGHT/4),Color.White);
				spriteBatch.DrawString (font,"All fucking music made by Tiago Campos", new Vector2 (10,window_HEIGHT - 100),Color.White);
			}
			if(gamestate == GameState.Running)
			{
				map.Draw(spriteBatch);
				foreach(Enemy enemy in enemies)
					enemy.Draw(spriteBatch);
				foreach(Shoot shoot in shootsFired)
					shoot.Draw(spriteBatch);
				ship.Draw(spriteBatch);
				particleSystem.Draw(spriteBatch);
				//HUD Drawing
				spriteBatch.DrawString (font,"Lives: " + (ship.Lives < 0 ? 0 : ship.Lives) , new Vector2 (0,0),Color.White);

			}
			spriteBatch.End();
			base.Draw (gameTime);
		}

		private void disposeSounds()
		{
			menuMusicInstance.Dispose();
			explosionSoundInstance.Dispose();
			snd_shootInstance.Dispose();
			mainMusicInstance.Dispose();
		}



	}
}

