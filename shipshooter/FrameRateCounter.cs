using System;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace shipshooter
{
	public class FrameRateCounter 
	{
		ContentManager content;
		SpriteFont spriteFont;

		int frameRate = 0;
		int frameCounter = 0;
		TimeSpan elapsedTime = TimeSpan.Zero;


		public FrameRateCounter(SpriteFont font)
		{
			spriteFont = font;
		}



		protected void UnloadGraphicsContent(bool unloadAllContent)
		{
			if (unloadAllContent)
				content.Unload();
		}


		public  void Update(GameTime gameTime)
		{
			elapsedTime += gameTime.ElapsedGameTime;

			if (elapsedTime > TimeSpan.FromSeconds(1))
			{
				elapsedTime -= TimeSpan.FromSeconds(1);
				frameRate = frameCounter;
				frameCounter = 0;
			}
		}


		public  void Draw(GameTime gameTime,SpriteBatch spriteBatch)
		{
			frameCounter++;

			string fps = string.Format("fps: {0}", frameRate);

	

			spriteBatch.DrawString(spriteFont, fps, new Vector2(33, 33), Color.Black);
			spriteBatch.DrawString(spriteFont, fps, new Vector2(32, 32), Color.White);


		}
	}
}

