#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;

#endregion

namespace shipshooter
{
	static class Program
	{
		private static Game1 game;

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main ()
		{
			Program.displaySSDLogo ();
			game = new Game1 ();
			game.Run();
			game.Dispose();
		}

		public static void displaySSDLogo()
		{

			Console.WriteLine( "            ..NMMMMMMMMMM8:.             ");
			Console.WriteLine( "            MMMMMMMMMMMMMMMMM8.          ");
			Console.WriteLine( "      . .   ,MMMMMMMMMMMMMMMMMMN         ");
			Console.WriteLine( "    . MMN.  .:   .    ....MMMMMMMO       ");
			Console.WriteLine( "    SMMMMM,...+SZZZZZZZS....:MMMMMM      ");
			Console.WriteLine( "   SMMMMM..:ZZZZZZZZZZZZZZZ:..7MMMMM.    ");
			Console.WriteLine( " .MMMMMZ..ZZZZZZ:.....?ZZZZZZ...MMMMM.   ");
			Console.WriteLine( " .,MMM,.7ZZZZ~.  MMMMM ..~ZZZZ,..MMMMN.  ");
			Console.WriteLine( "    .:.=ZZZO..MMMMMMMMMM~..SZZZ+..MMMM   ");
			Console.WriteLine( "    ..,ZZZ?..MMMMMMMMMMMMM..OZZZ..MMMMI  ");
			Console.WriteLine( "MMND..ZZZZ..MMMMMMMMMMMMMMM .ZZZ?.8MMMM. ");
			Console.WriteLine( "MMMM..ZZZS =MMMMMMMMMMMMMMM~.7ZZZ.~MMMM  ");
			Console.WriteLine( "MMMM..ZZZ7.SMMMMMMMMMMMMMMMO.=ZZZ.,MMMM  ");
			Console.WriteLine( "MMMM..ZZZS.+MMMMMMMMMMMMMMM=.IZZZ.7MMMM  ");
			Console.WriteLine( "MMMM..ZZZZ..MMMMMMMMMMMMMMM..ZZZI. ~S8N. ");
			Console.WriteLine( "MMMM8.~ZZZ=.:MMMMMMMMMMMMM..ZZZZ..       ");
			Console.WriteLine( "+MMMM..IZZZS..MMMMMMMMMMZ..SZZZI.? .     ");
			Console.WriteLine( ".MMMMM .ZZZZZ,...8MMM8 ..,ZZZZ~.SMMM+ .  ");
			Console.WriteLine( "  MMMMM..,ZZZZZZ,.....:SZZZZZ:.=MMMMI .  ");
			Console.WriteLine( " ..MMMMM ..+ZZZZZZZZZZZZZZZ+..MMMMMM.    ");
			Console.WriteLine( "   .MMMMMM....7ZZZZZZZZZ?....MMMMM,.     ");
			Console.WriteLine( "   ..8MMMMMM:.............  ..8MM ..     ");
			Console.WriteLine( "    .. MMMMMMMMMM8IIDMMMMM:    . .       ");
			Console.WriteLine( "         8MMMMMMMMMMMMMMMMM              ");
			Console.WriteLine( "            ?NMMMMMMMMMMMI               ");
			Console.WriteLine( "            ..  .,::,. ...    ");
			Console.WriteLine( );
			Console.WriteLine( " SPIN SOFTWARE DEVELOPMENT - SSD");
			Console.WriteLine();
		}
	}
}
