using System;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace shipshooter
{
	public class Shoot
	{
		Texture2D texture;
		Rectangle drawRectangle;
		bool active;



		
		public Shoot (Vector2 position,ContentManager content,int windowHeight)
		{
			texture = content.Load<Texture2D>("shoot");


			drawRectangle = new Rectangle((int) position.X,(int) position.Y,texture.Width/2,texture.Height/2);

			active = true;


		}

		public void Update(GameTime gametime)
		{
			drawRectangle.Y -= 10;
			if(drawRectangle.Bottom < 0)
				active = false;
		}


		public void Draw(SpriteBatch sprib)
		{
			sprib.Draw(texture,drawRectangle,Color.White);
		}

		public Rectangle getDrawRetangle()
		{
			return drawRectangle;
		}

		public bool Active
		{
			get { return active; }
			set { active = value; }
		}
	}
}

